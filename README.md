# OneLifeBuild

This project includes CI/CD instructions for building the game
[One Hour One Life](http://onehouronelife.com/)(OHOL). These instructions were
gleaned by reading Jason Roher's build scripts and reimplementing them "the
GitLab way".

Honestly, this mainly fulfills two purposes. A 64-bit build for my Chromebook's
Crostini, and playing with shared build steps between Mac/Linux on GitLab. The
latter was surprisingly successful. Maybe I'll do Windows next.

For installation instructions, check out [Install](Install.md).